

const backDrop = document.querySelector('.backdrop');
const button = document.querySelectorAll('.button');
const modal = document.querySelector('.modal');
const closeModal = document.querySelector('.modal__action--negative');
const mobileNav = document.querySelector('.mobile-nav')
const hamburgerBtn = document.querySelector('.toggle-button');

//const ctaBtn = document.querySelector('.main-nav__item--cta');

const addBackDropHandler = () => {
    backDrop.style.display = 'block';
    setTimeout(function () { backDrop.classList.add('open'); }, 20)
    modal.classList.add('open');
}

const closeModalHandler = () => {
    if (modal) {
        modal.classList.remove('open');
        mobileNav.classList.remove('open');
        backDrop.classList.remove('open');
        setTimeout(function () { backDrop.style.display = 'none'; }, 300)
    }
}

const showMobileMenuHandler = () => {
    mobileNav.classList.add('open');
    backDrop.style.display = 'block';
    setTimeout(function () { backDrop.classList.add('open'); }, 20)
}

for (let i = 0; i < button.length; i++) {
    button[i].addEventListener('click', addBackDropHandler)

}

if (closeModal) {
    closeModal.addEventListener('click', closeModalHandler);
}


backDrop.addEventListener('click', closeModalHandler);
hamburgerBtn.addEventListener('click', showMobileMenuHandler);

// ctaBtn.addEventListener('animationstart', function (event) {
//     console.log('Animation started', event)
// })

// ctaBtn.addEventListener('animationend', function (event) {
//     console.log('Animation ended', event)
// });

// ctaBtn.addEventListener('animationiteration', function (event) {
//     console.log('Animation iteration', event)
// })